// Copyright Epic Games, Inc. All Rights Reserved.

#include "ART3DTestGameMode.h"
#include "ART3DTestPlayerController.h"
#include "ART3DTestCharacter.h"
#include "UObject/ConstructorHelpers.h"

AART3DTestGameMode::AART3DTestGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AART3DTestPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/Core/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/Core/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}