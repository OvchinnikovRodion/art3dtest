// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ART3DTestGameMode.generated.h"

UCLASS(minimalapi)
class AART3DTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AART3DTestGameMode();
};



