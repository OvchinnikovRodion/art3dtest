// Copyright Epic Games, Inc. All Rights Reserved.

#include "ART3DTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ART3DTest, "ART3DTest" );

DEFINE_LOG_CATEGORY(LogART3DTest)
 